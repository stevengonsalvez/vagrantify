MQ_URL=http://public.dhe.ibm.com/ibmdl/export/pub/software/websphere/messaging/mqadv/mqadv_dev80_linux_x86-64.tar.gz 
MQ_PACKAGES="MQSeriesRuntime-*.rpm MQSeriesServer-*.rpm MQSeriesMsg*.rpm MQSeriesJava*.rpm MQSeriesJRE*.rpm MQSeriesGSKit*.rpm" 
MQ_QMGR_NAME="QM1"

#yum installs
#sudo yum install -y dos2unix
#sudo yum install -y mlocate
#sudo yum install -y telnet
#sudo yum install -y screen
#sudo yum install -y vim
#sudo yum install -y git
#sudo yum install -y wget
sudo yum update -y

##switching off iptables
sudo chkconfig iptables off
sudo /etc/init.d/iptables stop
sudo service firewalld stop

cd /tmp
#activate this statement if downloading from the internet.
#curl -LO $MQ_URL 
sudo tar -zxvf ./*.tar.gz 
sudo groupadd --gid 1414 mqm 
sudo useradd --uid 1414 --gid mqm --home-dir /var/mqm mqm 
sudo usermod -G mqm root

cd /tmp/server
sudo ./mqlicense.sh -text_only -accept
sudo rpm -ivh $MQ_PACKAGES
sudo sed -i -e "s;CheckShellDefaultOptions$;#CheckShellDefaultOptions;g" /opt/mqm/bin/mqconfig

sudo /tmp/mq-license-check.sh
sudo -s source /opt/mqm/bin/setmqenv -s
sudo echo "source /opt/mqm/bin/setmqenv -s" >> /var/mqm/.bash_profile

if [ ! "$(ls -A /var/mqm)" ]; then
	sudo /opt/mqm/bin/amqicdir -i -f
fi

sudo -H -u mqm bash -c 'source /opt/mqm/bin/setmqenv -s;crtmqm $MQ_QMGR_NAME;strmqm $MQ_QMGR_NAME'
sudo chmod 777 /tmp/config.mqsc

sudo -H -u mqm bash -c 'source /opt/mqm/bin/setmqenv -s;runmqsc $MQ_QMGR_NAME < config.mqsc'


### TO do 
# fix the connection to the qmgr - turn off chlauth and connauth