#All the parameters that are set
user_to_run_mule="mule"
mule_ce_location="https://repository-master.mulesoft.org/nexus/content/repositories/releases/org/mule/distributions/mule-standalone/3.6.1/mule-standalone-3.6.1.tar.gz"
mule_ee_location=""
logonuser="mulesoft"
logonpassword="muleinstance"


### Enabling users and password (need to find the exact line)
sudo sed -i -e 's|#PasswordAuthentication yes|PasswordAuthentication yes|' /etc/ssh/sshd_config
sudo sed -i -e 's|#PasswordAuthentication no|PasswordAuthentication yes|' /etc/ssh/sshd_config
sudo sed -i -e 's|PasswordAuthentication no|PasswordAuthentication yes|' /etc/ssh/sshd_config

#prevent root login

sudo tee -a /etc/ssh/sshd_config >/dev/null <<EOF
PermitRootLogin no
EOF



sudo service sshd restart

#creating new user
sudo adduser ${logonuser}
echo -e "$logonpassword\n$logonpassword\n" | sudo passwd $logonuser

#adding users to sudo file

sudo tee -a /etc/sudoers >/dev/null <<EOF
## add user to /etc/sudoers file 
${logonuser}      ALL=(ALL)       ALL
EOF


#yum installs
sudo yum install -y dos2unix

## install either/or or both
sudo yum install -y java-1.7.0-openjdk.x86_64
#installation path for jdk 7 /usr/lib/jvm/java-1.7.0-openjdk-1.7.0.101-2.6.6.1.el7_2.x86_64/jre/bin/java
#jmx path /usr/lib/jvm/java-1.7.0-openjdk-1.7.0.101-2.6.6.1.el7_2.x86_64/jre/lib/management

sudo yum install -y java-1.8.0-openjdk
#installation path for jdk 8 /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.91-0.b14.el7_2.x86_64/jre/bin/java

sudo yum install -y mlocate
sudo yum install -y telnet
sudo yum install -y vim
sudo yum install -y git
sudo yum install -y wget
sudo yum install -y unzip
sudo yum update -y

##switching off iptables
sudo chkconfig iptables off
sudo /etc/init.d/iptables stop
sudo service firewalld stop


##retrieving package of mule from the internet
#wget ${mule_ee_location}
cd /tmp
sudo tar xzf mule-ee-*
sudo useradd ${user_to_run_mule}
sudo mkdir /opt/mule
sudo chown -R ${user_to_run_mule}:${user_to_run_mule} /opt/mule
sudo mv mule-enterprise-standalone-*/* /opt/mule/
sudo chown -R ${user_to_run_mule}:${user_to_run_mule} /opt/mule/

#Replacing the Run as user to mule
sudo sed -i -e 's|#RUN_AS_USER=|RUN_AS_USER='${user_to_run_mule}'|' /opt/mule/bin/mule
sudo mv -f /tmp/wrapper.conf /opt/mule/conf/wrapper.conf 
sudo chown -R mule:mule /opt/mule/conf/wrapper.conf
sudo chmod 777 /tmp/adapter-ee-license.lic



### add below entries into the /etc/security/limits.conf

sudo tee -a /etc/security/limits.conf >/dev/null <<EOF
@mule   hard nofile 32800
@mule   soft nofile 32800
@mule   hard nproc 3617451
@mule   soft nproc 3617451
EOF

sudo updatedb

for each in `6`;do 
sudo cp $(dirname $each)/jmxremote.password.template $(dirname $each)/jmxremote.password   ; 

sudo tee -a $(dirname $each)/jmxremote.password >/dev/null <<EOF
# entries made to change the JMX configuration for mule
jconsole jconsole
EOF

sudo tee -a $(dirname $each)/jmxremote.access >/dev/null <<EOF
jconsole    readwrite
EOF

sudo tee -a $(dirname $each)/management.properties >/dev/null <<EOF
# entries made to change the JMX configuration for mule
com.sun.management.jmxremote.authenticate=true
com.sun.management.jmxremote.ssl=false
EOF

sudo chown ${user_to_run_mule}:${user_to_run_mule} $(dirname $each)/jmxremote.access $(dirname $each)/jmxremote.password
sudo chmod 600 $(dirname $each)/jmxremote.access $(dirname $each)/jmxremote.password

done

### need to add the line of applying the license file /opt/mule/bin/mule --installLicense /tmp/adapter-ee-license.lic; from vagrant by switching to mule user
sudo runuser -l  ${user_to_run_mule} -c '/opt/mule/bin/mule --installLicense /tmp/adapter-ee-license.lic'


## need to copy the domain as well - comment out if you dont have domains.
sudo chown ${user_to_run_mule}:${user_to_run_mule} /tmp/prod.zip /tmp/intb.zip
sudo runuser -l  ${user_to_run_mule} -c 'cp /tmp/prod.zip /tmp/intb.zip /opt/mule/domains'


## install nmon https://gist.github.com/stevengonsalvez/75f04b9e4ac27154cf31ed1c3983adc1
sudo wget http://nmon.sourceforge.net/docs/MPG_nmon_for_Linux_14a_binaries.zip -P /tmp
sudo unzip -o /tmp/MPG_nmon_for_Linux_14a_binaries.zip
sudo cp /tmp/nmon_x86_64_centos5 /usr/local/bin/
sudo chmod a+x /usr/local/bin/nmon_x86_64_centos5
sudo ln -s /usr/local/bin/nmon_x86_64_centos5 /usr/local/bin/nmon


sudo cp /tmp/mule-init.sh /etc/init.d/mule
sudo chmod 755 /etc/init.d/mule
sudo chkconfig --add mule
sudo service mule start