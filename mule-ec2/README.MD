### Steps to run
- refer to [link](http://www.cantoni.org/2014/09/22/quick-guide-vagrant-amazon-ec2)
- vagrant plugin install vagrant-aws
- vagrant box add dummy https://github.com/mitchellh/vagrant-aws/raw/master/dummy.box
- First copy the export.sh and use the required values in there
- execute the script export_format.sh