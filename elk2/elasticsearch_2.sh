#!/bin/bash
# place the package elasticsearch-1.4.4.rpm or whatever is the latest version in the ./files folder or you can do a wget and alter the script to dynamically download as well.
# Also download the latest java version - preverabbly do a apt-get for openjdk 1.7 - Refer to the graylog2 install in the other repository

#yum installs
sudo yum install -y dos2unix
sudo yum install -y java-1.7.0-openjdk.x86_64
sudo yum install -y mlocate
sudo yum install -y telnet
sudo yum install -y screen
sudo yum install -y vim
sudo yum install -y git
sudo yum install -y wget
sudo yum update -y

# Set variables
FILES=Vagrant/files

# Java is already installed from provisioning.sh - if  you have a custom version of java , place it in files and use it from here
# Install java 7
#if [ "$1" == "64" ]; then
#       jre="jre-7u75-linux-x64.tar.gz"
#else
#       jre="jre-7u75-linux-i586.tar.gz"
#fi

#tar -C /usr/local -zxf ${FILES}/${jre}
#ln -snf /usr/local/jre1.7.0_75 /usr/local/java

# Check wether elasticsearch is already installed
# Download elasticsearch rpm

sudo rpm --import https://packages.elastic.co/GPG-KEY-elasticsearch
 
sudo bash -c 'cat >/etc/yum.repos.d/elasticsearch.repo << EOL
[elasticsearch-2.x]
name=Elasticsearch repository for 2.x packages
baseurl=http://packages.elastic.co/elasticsearch/2.x/centos
gpgcheck=1
gpgkey=http://packages.elastic.co/GPG-KEY-elasticsearch
enabled=1
EOL
'


# Install elasticsearch from provided .deb package
sudo yum install -y elasticsearch

        # To install from file comment the above line and uncomment the one below
        # rpm -ivh Vagrant/files/elasticsearch-1.4.4.noarch.rpm

        # Add elasticsearch to startupscripts and start it
        #update-rc.d elasticsearch defaults 95 10

       sudo  chkconfig --add elasticsearch
       sudo  service elasticsearch start

      	sudo echo "Waiting for elasticsearch to startup ..."
        until curl -s --connect-timeout 1 localhost:9200; do
                echo "."
                sleep 1
        done
		
	
	

        # Import sample data
        echo "Importing sample data ..."
         curl -s -XPUT 'localhost:9200/_snapshot/sampledata/' -d '{
                "type": "fs",
                "settings": {
                        "location": "/vagrant/sampledata/",
                        "compress": true
                }
        }'
        curl -s -XPOST 'localhost:9200/_snapshot/sampledata/v1/_restore'
        echo "Finished importing sample data!"


# Installing HQ plugin for the management of elastic search
sudo /usr/share/elasticsearch/bin/plugin install royrusso/elasticsearch-HQ
	


# Installing searchguard
#sudo bin/plugin install com.floragunn/search-guard-2/2.2.0.0-alpha2	

#Install searchguard ssl
#sudo bin/plugin install com.floragunn/search-guard-ssl/2.2.0.6
	

# http basic auth plugin - Does not work with 2.0


# TO DO ************* for inline script need to aadd the below 
# permission org.elasticsearch.script.ClassPermission "sun.reflect.MethodAccessorImpl"; within the below file
#modules/lang-groovy/src/main/plugin-metadata/plugin-security.policy

#add configuration details to elasticsearch.yml - if you need , edit the file manually later
sudo mv /etc/elasticsearch/elasticsearch.yml /etc/elasticsearch/elasticsearch.original.yml

sudo echo " this is the current folder we are in"
sudo pwd

sudo cp /home/vagrant/vagrant/elk2/elasticsearch.yml /etc/elasticsearch/elasticsearch.yml

sudo service elasticsearch restart




