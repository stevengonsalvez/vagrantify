
sudo wget http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-5.noarch.rpm -P /tmp
sudo rpm -ivh /tmp/epel-release-7-5.noarch.rpm
sudo yum -y install epel-release
sudo yum -y install nginx httpd-tools

# below command will get stuck as it needs user input for the password. 
sudo htpasswd -c /etc/nginx/htpasswd.users kibanaadmin


systemctl start nginx
systemctl enable nginx



# there is a issue where you get 13: permission denied because of sercurity policies - to turn it off 
#  refer the link http://stackoverflow.com/questions/23948527/13-permission-denied-while-connecting-to-upstreamnginx
sudo setsebool httpd_can_network_connect on
sudo setsebool httpd_can_network_connect on -P
# and to check if it is off
getsebool -a | grep httpd