#!/bin/bash
# chkconfig: 345 91 10
# description: mmc/tomcat Start Stop
# processname: mmc/tomcat

#
# Copy this script to /etc/init.d
# Run: chmod 755 /etc/init.d/mmc
# Run: chkconfig --add mmc
#

# Source function library.
. /etc/init.d/functions
RETVAL=$?

CATALINA_HOME=/opt/mmc
case "$1" in
start)
if [ -f $CATALINA_HOME/bin/startup.sh ];
then
  echo $"Starting MMC/Tomcat"
  cd $CATALINA_HOME
  /bin/su -s /bin/bash mule bin/startup.sh
fi
;;
stop)
if [ -f $CATALINA_HOME/bin/shutdown.sh ];
then
  echo $"Stopping MMC/Tomcat"
  cd $CATALINA_HOME
  /bin/su mule bin/shutdown.sh
fi
;;
*)
echo $"Usage: $0 {start|stop}"
exit 1
;;
esac

exit $RETVAL