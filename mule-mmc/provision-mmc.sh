echo "provisioning mmc"

#All the parameters that are set
tomcat_url=http://mirror.ox.ac.uk/sites/rsync.apache.org/tomcat/tomcat-7/v7.0.65/bin/apache-tomcat-7.0.65.tar.gz
user_to_run_mule="mule"

#yum installs
sudo yum install -y dos2unix
sudo yum install -y java-1.7.0-openjdk.x86_64
sudo yum install -y mlocate
sudo yum install -y telnet
sudo yum install -y git
#sudo yum install -y wget
sudo yum update -y


cd /tmp
sudo useradd ${user_to_run_mule}
sudo curl -LO ${tomcat_url}
sudo tar xzf apache-tomcat-*.tar.gz
sudo mkdir -p /opt/mule
sudo chown -R ${user_to_run_mule}:${user_to_run_mule} /opt/mule
sudo ln -s /opt/mule /opt/mmc
sudo chown -h ${user_to_run_mule}:${user_to_run_mule} /opt/mmc
sudo mv apache-tomcat-*/* /opt/mule/
sudo mv mmc-*.war /opt/mule/webapps/mmc.war
sudo cp setenv.sh /opt/mule/bin/
sudo chmod 755 /opt/mule/bin/setenv.sh
sudo mv mmc.sh /etc/init.d/mmc
sudo chmod 755 /etc/init.d/mmc
cd /opt/mule

sudo chown -hR ${user_to_run_mule}:${user_to_run_mule} /opt/mule

sudo sed -i -e 's#<Connector port="8080"#<Connector port="8585"#g' /opt/mule/conf/server.xml

sudo chkconfig --add mmc
sudo service mmc start