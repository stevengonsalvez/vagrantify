
sudo yum install -y dos2unix
sudo yum install -y java-1.7.0-openjdk.x86_64
sudo yum install -y mlocate
sudo yum install -y telnet
sudo yum install -y git
#sudo yum update -y
sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo
sudo rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key
sudo yum install -y jenkins

##switching off iptables
sudo chkconfig iptables off
sudo /etc/init.d/iptables stop
sudo service firewalld stop

sudo chkconfig jenkins on
sudo service jenkins start

#section to update git if on centos 6
sudo rpm -i 'http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.3-1.el6.rf.x86_64.rpm'
sudo rpm --import http://apt.sw.be/RPM-GPG-KEY.dag.txt
sudo sed -i -e 's|enabled=0|enabled=1|' /etc/yum.repos.d/rpmforge.repo
sudo sed -i -e 's|enabled= 0|enabled= 1|' /etc/yum.repos.d/rpmforge.repo
sudo yum update -y git